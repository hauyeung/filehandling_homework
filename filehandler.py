import os

def countfiles(path):
    filescount = {}
    exts = set()    
    fileslist = set(os.listdir(path))
    for f in fileslist:
        if os.path.isfile(os.path.join(path,f)) ==True:
            parts = f.split('.')
            if len(parts) >1:
                exts.add(parts[-1])
            elif len(parts) ==1:
                exts.add('')
                
    for x in exts:
        filescount[x]= 0
            
    for f in fileslist:        
        parts = f.split('.')
        if os.path.isfile(os.path.join(path,f)) ==True:
            if len(parts) >1:
                x = parts[-1]
            elif len(parts) ==1:
                x = ''
            filescount[x]+=1

    return filescount
