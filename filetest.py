import shutil, os, unittest, filehandler

class filestest(unittest.TestCase):
    def setUp(self):
        self.test = 'test'
        self.test2 = 'test2'
        os.chdir(os.path.dirname(__file__))
        if not os.path.exists(self.test):
            os.mkdir(self.test)
        if not os.path.exists(self.test2):
            os.mkdir(self.test2)
            
    """Test file count with multiple files"""
    def test_filescount(self):
        os.chdir(os.path.dirname(__file__))
        os.chdir(self.test)
        open('file.abc','w').close()    
        open('file.txt','w').close()
        if not os.path.exists('subfolder'):
            os.mkdir('subfolder')    
        os.chdir(os.path.dirname(__file__))
        self.assertEqual({'abc': 1, 'txt': 1}, filehandler.countfiles(self.test))
        
    """Test file count with multiple files and subfolders"""
    def test_filecount2(self):
        os.chdir(os.path.dirname(__file__))
        os.chdir(self.test2)
        open('file.abc','w').close()     
        open('file2.abc','w').close()
        open('file2.cde.cde.abc','w').close()     
        open('file.txt','w').close()
        open('file','w').close()
        open('newfile','w').close()
        if not os.path.exists('subfolder'):
            os.mkdir('subfolder')
        if not os.path.exists('subfolder2'):
            os.mkdir('subfolder2')     
        if not os.path.exists('bogus.bogus.bogus'):
            os.mkdir('bogus.bogus.bogus')  
        os.chdir(os.path.dirname(__file__)) 
        self.assertEqual({'':2,'abc': 3, 'txt': 1}, filehandler.countfiles(self.test2))
        
    def tearDown(self):
        os.chdir(os.path.dirname(__file__))
        shutil.rmtree(self.test)
        shutil.rmtree(self.test2)
        
if __name__ == '__main__':
    unittest.main()